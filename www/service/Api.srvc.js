(function() {
    'use strict';
    angular.module('myPageApp').factory('ApiSrvc', ['httpSrvc', '$q', ApiSrvc]);

    function ApiSrvc(httpSrvc, $q) {

        var get = function(url) {
            var defer = $q.defer(); //promisse to handle async calls

            httpSrvc.http().get({
                url: url
            }).$promise.then(
                function(data) {
                    defer.resolve(data);
                },
                function(error) {
                    defer.reject(error);
                });
            return defer.promise;
        };

        var post = function(category, action, query) {
            var defer = $q.defer(); //promisse to handle async calls

            httpSrvc.http().save({
                category: category,
                action: action,
            }, query).$promise.then(
                function(data) {
                    defer.resolve(data);
                },
                function(error) {
                    defer.reject(error);
                });
            return defer.promise;
        };

        return {
            get: get,
            post: post
        };
    }
})();
