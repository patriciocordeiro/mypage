/*Myservice.srvc.js*/
(function() {
    'use strict';
    angular.module('myPageApp').factory('Myservice', ['$q', 'ApiSrvc', Myservice]);

    function Myservice($q, ApiSrvc) {



        var msg = function(msg) {
            var defer = $q.defer();
            var query = {};
            query.msg = msg;
            ApiSrvc.post('user', 'message', query).then(function(res) {
                console.log(res);
                if (!res.success && !res.err) {
                    /*No data from server (down)*/
                    defer.reject('server down');
                } else {
                    /*Some data  */
                    if (res.err) {
                        //TODO: define every type of error
                        /*receive an Error*/
                        defer.reject('error');
                    } else if (res.success) {
                        /*all ok*/
                        console.log('sucesso');
                        defer.resolve('success');
                    }
                }
            });
            return defer.promise;
        };



        return {
            msg: msg,

        };
    }
}());
