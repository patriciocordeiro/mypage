/*Resource.srvc.js*/
(function() {
    "use strict";

    angular.module('myPageApp').factory('httpSrvc', ['$resource', httpSrvc]);

    function httpSrvc($resource, ConfigSrvc) {
        //        var site = 'https://dry-wildwood-25351.herokuapp.com';
        // var site = ''
        //var site = 'http://192.168.0.10:5000';
        //        var site = 'http://200.239.73.237:3000';
        var site = 'https://patriciocordeiro.herokuapp.com'

        var http = function() {
            return $resource(site + '/:category/:action', {
                category: '@category',
                action: '@action'
            }, {
                'get': {
                    method: 'GET',
                    isArray: true
                },
                'save': {
                    method: 'POST',
                    isArray: false
                }
            });
        };

        return {
            http: http,
        };
    }

}());
