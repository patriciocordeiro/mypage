'use strict'
angular.module("myPageApp", ['ui.router', 'ngMaterial', 'ngResource', 'ngMessages'])
    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: "^",
                abstract: true,
                template: '<ui-view/>',

            })
            .state('app.home', {
                //                abstract: true,
                url: "/home",
                templateUrl: './views/home.html',
                controller: 'Ctrl as vm',
            })

        .state('app.works', {
            //                abstract: true,
            url: "/works",
            templateUrl: './views/works.html',
            controller: 'Ctrl as vm',
        })

        .state('app.experience', {
                //                abstract: true,
                url: "/experience",
                templateUrl: './views/experience.html',
                controller: 'Ctrl as vm',
            })
            .state('app.education', {
                //                abstract: true,
                url: "/education",
                templateUrl: './views/education.html',
                controller: 'Ctrl as vm',
            })
            .state('app.compskills', {
                //                abstract: true,
                url: "/compskills",
                templateUrl: './views/compskills.html',
                controller: 'Ctrl as vm',
            })
            .state('app.contacts', {
                url: "/contacts",
                templateUrl: './views/contacts.html',
                controller: 'Ctrl as vm',
            });



        //if no state redirect to home
        $urlRouterProvider.otherwise('/home');

    });
