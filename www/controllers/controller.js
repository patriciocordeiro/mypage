(function() {
    'use strict';

    angular.module('myPageApp').controller('Ctrl', ['$mdDialog', '$mdSidenav', '$mdMedia', 'Myservice', Ctrl]);


    function Ctrl($mdDialog, $mdSidenav, $mdMedia, Myservice) {
        var vm = this;
        vm.currentNavItem = 'home';
        vm.contact = {}; // for contact form

        vm.contactForm = {}; // form object
        vm.mdMedia = $mdMedia;


        vm.toggleLeft = buildToggler('left');
        vm.toggleRight = buildToggler('right');

        function buildToggler(componentId) {
            return function() {
                $mdSidenav(componentId).toggle();
            };
        }



        vm.aboutme = [{
            title: 'Sobre mim',
            text: 'Meu nome é Patrício Cordeiro, engenheiro eletricista com mestrado e  graduação pela Universidade Federal do Pará.  Atualmente sou professor na UFPA, onde leciono as disciplinas: Eletrônica Digital e microprocessadores/microcontroladores.' + '     Possui experiência em Processamento Digital de Sinais, projeto, simulação e desenvolvimento prático de circuitos digitais e sistemas embarcados usando FPGA e micro-controladores; Projeto, simulação e desenvolvimento prático de circuitos analógicos e de RF; Instrumentação e medição em laboratório: Experiência avançada em Analisadores de espectro, gerador de sinais de ondas arbitrárias e padronizadas, Osciloscópios, Analisadores lógicos etc. Projeto, desenvolvimento, montagem e testes em placas de circuito impresso de média e alta complexidade; Desenvolvimento web: Projeto de paginas web avançados e dinâmicos, projeto de aplicativos para aparelhos móveis usando AngularJS, Ionic, Bootstrap, HTML, Javascript, CSS, Jquery, NodeJs, etc. Possui conhecimento em sistemas de comunicação móveis: GSM e LTE. Principais áreas de interesse: Processamento digital de sinais, circuitos digitais, sistemas embarcados, circuitos elétricos e eletrônica, microcontroladores e microprocessadores, circuitos analógicos e de rádio frequência, telecomunicações e desenvolvimento web.'
        }, {
            title: 'Áreas de interesse',
            text: 'Atuar como programador front-end e backend no desenvolvimento de aplicativos web e móveis, Processamento digital de sinais, circuitos digitais, sistemas embarcados usando FPGA, circuitos elétricos e eletrônica, microcontroladores e microprocessadores, circuitos analógicos e de rádio frequência, telecomunicações'
        }]

        vm.softwares = [
            { title: 'Mentor Graphics Dx Designer  e Expedition enterprise e Hyperlynx SI e PI', icon: 'mentor' },
            { title: 'Agilent ADS e Systemvue e VSA 89600', icon: 'agilent' },
            { title: 'Altera Quartus II e ModelSim e DSP Builder e NIOS II', icon: 'altera' },
            { title: ' Adobe dreamweaver', icon: 'adobe_dreamweaver' },
            { title: 'NetBeans' },
            { title: 'Eclipse' },
            { title: 'Proteus' },
            { title: 'Arduino IDE' },
            { title: 'National Instruments Labview' },
            { title: 'Matlab' },
            { title: 'Matlab' }
        ]

        vm.sofGraphics = [

            { title: ' Adobe Illustrator', icon: 'adobe_illustrator' },
            { title: ' Adobe Photoshop', icon: 'adobe_photoshop' },
            { title: ' Microsoft Visio', icon: 'visio' },

        ]

        vm.navbarItem = [{
            url: 'app.home',
            name: 'home',
            title: 'Home'
        }, {
            url: 'app.education',
            name: 'education',
            title: 'Formação'
        }, {
            url: 'app.experience',
            name: 'experience',
            title: 'Experiência profissional'
        }, {
            url: 'app.compskills',
            name: 'compskills',
            title: 'Habilidades Computacionais'
        }, {
            url: 'app.works',

            name: 'projects',
            title: 'Projetos'
        }, {
            url: 'app.contacts',

            name: 'contacts',
            title: 'Contato'
        }]

        vm.courses = [{
            title: ' Labview Hands on course',
            year: '2006'
        }, {
            title: 'VHDL avançado',
            year: '2010'
        }, {
            title: ' Quartus II Timing analyzer',
            year: '2010'
        }, {
            title: ' Quartus II Software Design Series: Verification',
            year: '2010'
        }, {
            title: ' Designing with the Nios II Processor and SOPC Builder',
            year: '2010'
        }, {
            title: 'Learn and understand AngularJS (udemy)',
            year: '2015'
        }, {
            title: ' All you need to know about angularjs – training on angularjs(udemy)',
            year: '2015'
        }, {
            title: ' The complete web developer course-build 14 websites(udemy))',
            year: '2015'
        }, {
            title: 'Besser- bits RF technology certification',
            year: '2015'
        }, {
            title: 'SCRUM – uma metodologia para gestão de projetos tecnológicos',
            year: '2015'
        }, {
            title: 'ISO 9001:2008 - Leitura e Interpretação da norma',
            year: '2015'
        }]


        vm.webskills = [{
            title: 'HTML 5',
            icon: 'html5',

        }, {
            title: 'Javascript',
            icon: 'javascript',

        }, {
            title: 'Angularjs',
            icon: 'angularjs',

        }, {
            title: 'Nodejs',
            icon: 'nodejs',

        }, {
            title: 'CSS3',
            icon: 'css3',

        }, {
            title: 'Ionic',
            icon: 'ionic',

        }, {
            title: 'Bootstrap',
            icon: 'bootstrap',

        }, {
            title: 'Java',
            icon: 'java',

        }, {
            title: 'VHDL',
            icon: 'vhdl',

        }, {
            title: 'C#',
            icon: 'cprogramming',

        }]



        vm.projects = [{
            title: 'BUMI',
            description: 'aplicativo mobile para busca de oficinas mecânicas',
            status: 'Em desenvolvimento',
            url: 'https://bumi.herokuapp.com/',
            icon: 'heroku'

        }, {
            title: 'PYM Eletrônica',
            description: 'Web site e-commerce para comércio de produtos de eletônica',
            status: 'Em desenvolvimento',
            url: 'https://pymeletronica.herokuapp.com',
            icon: 'heroku'
        }, {
            title: 'Quiz maker',
            description: 'Projeto  site e-commerce para comércio de produtos de eletônica',
            status: 'Em desenvolvimento',
            url: 'https://github.com/patriciocordeiro/quizmaker',
            icon: 'github'

        }, {
            title: 'Homme Controll',
            description: 'Aplicativo de automação residêncial que emprega Raspberry Pi para controlle, em tempo de real,  de aparelhos, lampadas, tomadas residencias',
            status: 'Concluído',
            url: 'https://bitbucket.org/patriciocordeiro/homecontroll',
            icon: 'bitbucket'
        }]

        /*Message*/
        vm.contact = {
            email: 'paponcio6@gmail.com',
            name: 'Mila',
            subject: 'Temos vaga para ti',
            text: 'Serás contratado imediatamente'
        }
        vm.sendMessage = function(ev, msg) {
            console.log(msg)
            vm.contact = {};
            vm.contactForm.$setPristine();
            vm.contactForm.$setUntouched();

            Myservice.msg(msg).then(function() {
                    $mdDialog.show(

                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Obrigado por me contactar!')
                        .textContent('Sua mensagem foi enviada com sucesso.')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(ev)
                    );
                },
                function() {
                    $mdDialog.show(
                        $mdDialog.alert()
                        .parent(angular.element(document.body))
                        .clickOutsideToClose(true)
                        .title('Erro ao enviar mensagem!')
                        .textContent('Desculpe, ocorreu algum erro. Tente novamente.')
                        .ariaLabel('Alert Dialog Demo')
                        .ok('OK')
                        .targetEvent(ev)
                    );
                });

        }
    }

})();
