(function() {
    'use strict';
    module.exports = function(app, express, userApi) {
        var router = express.Router();
        router.post('/user/message', userApi.contactMessage);


        app.use('/', router);
    };

})();
