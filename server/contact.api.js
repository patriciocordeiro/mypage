(function() {
    "use strict";
    var async = require('async');
    var emailService = require('./email-service');

    module.exports = {
        contactMessage: function(req, res) {
            console.log(req.body);
            if (req.body.msg) {
                emailService.sentEmailToUser(req.body.msg);
                res.json({
                    success: true,
                    data: 'ok'
                });
            } else {
                res.json({
                    err: 'fail'
                });
            }
        },

    };
}());
