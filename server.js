'use strict'
//load packages
var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var path = require('path');
var port = 5000;
app.set('port', process.env.PORT || port);
var contactApi = require('./server/contact.api');
//var mongoose = require('mongoose');
//var ObjectId = require('mongodb').ObjectID;


/*MONGODB--------------------------------------------------*/
//mongoose.connect('mongodb://localhost/pontoD');
//check if connected
//var db = mongoose.connection;
//db.on('error', console.error.bind(console, 'connection error:'));
//db.once('open', function (callback) {
//    console.log('connected to database')
//});
/*--------------------------------------------------------------*/


/*Express config*/
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
})); // get information from html forms
//use static files
app.use(express.static(path.join(__dirname, './www')));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/index.html'));
});

require('./server/contact.routes')(app, express, contactApi);


app.listen(process.env.PORT || port, function() {
    console.log('Quiz app listening on port', port);
});
